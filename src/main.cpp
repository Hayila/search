#include <iostream>
#include <string>
#include "search.hpp"

using namespace std;

int main()
{
    string text = "", pattern = "";


    cout<<"\nWelcome to the 'pattern searcher'\n\n\tPlease enter a text and a pattern (spaces are allowed in both strings) \n\n\t text >>> ";   
    getline(cin, text);
    
    cout<<"\n\t pattern >>> ";
    getline(cin, pattern);

     int result = search(text, pattern);
     if (result == -1)
       cout <<"\n\nSorry, the pattern was not found in the text\n";

      else
       cout <<"\n\nThe pattern was found at index "<<result<<" in the text\n";

}

